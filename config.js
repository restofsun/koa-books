module.exports = {
    app: {
        name: 'Books app',
    },
    server: {
        port: 3000,
        api: '/api/v1'
    },
    db: {
        host: 'localhost',
        database: 'books',
        user: 'root',
        debug: false
    }
}
