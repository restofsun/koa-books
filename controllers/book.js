const Books = require('../models/books')

module.exports.get = async ctx => {
    ctx.body = await Books.get(ctx.params.id);
}

module.exports.getAll = async ctx => {
    ctx.body = await Books.getAll();
}

module.exports.find = async ctx => {
    ctx.body = await Books.filter(ctx.request.body);
};

module.exports.create = async ctx => {
    ctx.body = await Books.create(ctx.request.body);
};

module.exports.remove = async ctx => {
    await Books.delete(ctx.params.id);
    ctx.body = { message: `Book #${ctx.params.id} was deleted` };
};

module.exports.update = async ctx => {
    ctx.body = await Books.update(ctx.params.id, ctx.request.body);
};
