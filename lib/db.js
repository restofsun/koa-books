const config = require('../config')

const poolConfig = {
    host: config.db.host,
    user: config.db.user,
    database: config.db.database,
    waitForConnections: true,
    connectionLimit: 100,
    queueLimit: 0,
    debug: config.db.debug
}

const mysql = require('mysql2/promise');
const pool = mysql.createPool(poolConfig);

module.exports = pool
