const wrapper = require('co-redis');
const crypto = require('crypto');
const Redis = require('redis');
const config = require('config');

let redisAvailable = false;
const client_ = Redis.createClient('redis://localhost:6379/');
const client = wrapper(client_);
const paths =  new Map();

const md5 = s => crypto.createHash('md5').update(s).digest('hex');
const arr = o => (Array.isArray(o) ? o : [o]);

client.on('end', () => { redisAvailable = false; });
client.on('connect', () => { redisAvailable = true; });
client.on('error', () => { redisAvailable = false; });

const cache = async (ctx, next) => {
    let result;
    let {url, path} = ctx.request;
    const spath = path.replace(config.get('api/v1'), '');

    const key = `recache:${spath}:${md5(url)}`;

    if (ctx.request.headers.hasOwnProperty('drop-cache')) drop(spath);
    if (ctx.request.headers.hasOwnProperty('drop-cache-all')) drop('');

    if (!paths.has(spath) || !redisAvailable) return await next();

    try { result = JSON.parse(await client.get(key)); } catch (e) { }

    if (result) {
        ctx.response.set('X-Koa-Cache', 'true');
        return result;
    }
    result = await next();
    try {
        await client.setex(key, paths.get(spath), JSON.stringify(result));
    } catch (e) { console.log(`cache error: ${e}`) }
    return result;
};

const drop = path => {
    // console.log(`cache: drop ${path||'all'}`);
    try {
        arr(path).map(p => client_.keys(`recache:${p}*`, (err, keys) => {
            if (keys.length) client_.del(keys)
        }));
    } catch (e) {}
};

const use = (model, path, expire=30*60) => {
    if (!path) return;
    // console.log(`cache: use  [${model.name}] ${path} ${expire}s`);
    path = arr(path);
    path.map(p => paths.set(p, expire));
    if (model) hook(model, path);
};

exports.cache = cache;
exports.use = use;
exports.drop = drop;

drop('');

