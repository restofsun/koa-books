const db = require('./db');

function basicQueries (table) {
    return {
        select: `SELECT * from ${table}`,
        insert: `INSERT INTO ${table} SET ? ON DUPLICATE KEY UPDATE ?`,
        update: `UPDATE ${table} SET ? WHERE id=?`,
        delete: `DELETE FROM ${table} WHERE id=?`,
        count:  `SELECT COUNT(*) from ${table}`,
    }
}

const MAX_WIDTH = 30

function truncate (input, max = MAX_WIDTH - 4) {
    return input.length > max ? `${input.substring(0, max)} ...` : input;
}

class Model {
    constructor (table) {
        this.queries = basicQueries(table)
    }
    async getAll () {
        return (await db.query(this.queries.select))[0];
    }
    async get (id) {
        return (await db.query(`${this.queries.select} WHERE id=?`,[Number(id)]))[0][0];
    }
    async filter (fields) {
        const sql = []
        const args = []

        let sort = ''


        if (fields.hasOwnProperty('id')) {
            sql.push('id=?')
            args.push(fields.id)
        }
        if (fields.hasOwnProperty('title')) {
            sql.push(`title LIKE '%${fields.title}%'`)
        }
        if (fields.hasOwnProperty('description')) {
            sql.push(`description LIKE '%${fields.description}%'`)
        }
        if (fields.hasOwnProperty('author')) {
            sql.push(`author LIKE '%${fields.author}%'`)
        }
        if (fields.hasOwnProperty('startDate')) {
            sql.push('date > ?')
            args.push(fields.startDate)
        }
        if (fields.hasOwnProperty('endDate')) {
            sql.push('date < ?')
            args.push(fields.endDate)
        }
        if (fields.hasOwnProperty('image')) {
            sql.push('image IS NOT NULL')
        }
        if (fields.hasOwnProperty('sort')) {
            sort += `ORDER BY ${fields.sort} `
            sort += fields.hasOwnProperty('sortDesc') ? 'DESC' : 'ASC'
        }

        const limit = `LIMIT ${fields.hasOwnProperty('count') ? Number(fields.count) : 20} OFFSET ${fields.hasOwnProperty('offset') ? Number(fields.offset) : 0}`

        const result = await db.query(`${this.queries.select} WHERE ${sql.join(' AND ')} ${sort} ${limit}`, args)
        return result[0]
    }
    async create (fields) {
        const result = await db.query(this.queries.insert,[fields, fields]);
        return this.get(result[0].insertId);
    }
    async update (id, fields) {
        await db.query(this.queries.update,[fields, Number(id)]);
        return this.get(id);
    }
    async delete (id) {
        const result = await db.query(this.queries.delete,[Number(id)]);
        return result.affectedRows;
    }
    static formatRows (rows) {
        const allRows = ['\n'].concat(rows)
        const formattedRows = allRows.map(row => Model.formatOneRow(row))
        return formattedRows.join('\n')
    }
    static formatOneRow (row) {
        return Object.values(row)
                     .map(field => truncate(String(field) || '-').padEnd(MAX_WIDTH))
                     .join('\t')
    }
}

module.exports = Model
