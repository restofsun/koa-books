const db = require('../lib/db');
const casual = require('casual');
const jdenticon = require('jdenticon');

const TABLE_NAME = 'books';

async function createBook () {
    const newBook = {
        title: casual.title,
        description: casual.description,
        author: casual.full_name,
        date: new Date(casual.unix_time),
        image: jdenticon.toPng(casual.word, 150),
    };

    return db.query(`INSERT INTO ${TABLE_NAME} SET ? ON DUPLICATE KEY UPDATE ?`,[newBook, newBook]);
}

async function init (count = 1e1) {
    await db.query('DROP DATABASE IF EXISTS `books`;');
    await db.query('CREATE DATABASE `books`');
    await db.query('USE `books`');
    await db.query('DROP TABLE IF EXISTS `books`.`books`;')
    await db.query('CREATE TABLE `books`.`books` (`id` serial,`title` varchar(255) NOT NULL,`author` varchar(255) NOT NULL,`description` text DEFAULT NULL,`date` datetime NOT NULL DEFAULT NOW(),`image` blob DEFAULT NULL, PRIMARY KEY (id));')

    for (let i = 0; i < count; i++) await createBook()
    return db.query('SELECT * FROM `books`.`books`;')
}

const MAX_WIDTH = 40

const truncate = (input, max = MAX_WIDTH - 4) => input.length > max ? `${input.substring(0, max)} ...` : input;

if (require.main === module) {
    init()
      .then((res) => {
          const [rows, fields] = res
          console.log(fields.map(e => e.name.padEnd(MAX_WIDTH)).join('\t').toUpperCase())
          console.log(''.padEnd(MAX_WIDTH * fields.length, '-'))
          console.log(rows.map(row => Object.values(row).map(field => truncate(String(field) || '-').padEnd(MAX_WIDTH)).join('\t')).join('\n'))
          console.log(''.padEnd(MAX_WIDTH * fields.length, '-'))
          console.log('done')
      })
      .catch((err) => console.error(err))
}

