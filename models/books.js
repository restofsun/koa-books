const Model = require('../lib/model')

const Books = new Model('books')
const SEPARATOR = '\n\n'.padEnd(60, '-')
module.exports = Books


async function main () {
    const casual = require('casual');
    const jdenticon = require('jdenticon');

    console.log('all:', Model.formatRows(await Books.getAll()), SEPARATOR)
    console.log('first:', Model.formatRows(await Books.get(1)), SEPARATOR)

    const newBook = await Books.create({
        title: casual.title,
        description: casual.description,
        author: casual.full_name,
        date: new Date(casual.moment),
        image: jdenticon.toPng(casual.word, 200),
    })

    console.log('new:', Model.formatRows(await Books.getAll()), SEPARATOR)

    newBook.description = casual.description
    const updatedBook = await Books.update(newBook.id, newBook)
    // console.log('updated:', Model.formatRows(updatedBook), SEPARATOR)
    console.log('update:', Model.formatRows(await Books.getAll()), SEPARATOR)

    await Books.delete(newBook.id)
    console.log('delete:', Model.formatRows(await Books.getAll()), SEPARATOR)

}

if (require.main === module) {
    main()
      .then(() => {})
      .catch(err => console.error(err))
}
// const [rows, fields] = await conn.execute('select ?+? as sum', [2, 2]);
