const Router = require('koa-router')
const config = require('../config')
const {get, getAll, find, create, update, remove} = require('../controllers/book');

const router = new Router({ prefix: config.server.api });

router
  .get('/book', getAll)
  .get('/book/:id', get)
  .post('/book/filter', find)
  .post('/book', create)
  .delete('/book/:id', remove)
  .put('/book/:id', update);

module.exports = router.routes();
