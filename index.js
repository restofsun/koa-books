const Koa = require('koa');
const Router = require('koa-router');
const Logger = require('koa-logger');
const Cors = require('kcors');``
const BodyParser = require('koa-bodyparser');

const error = require('./middleware/error');
const books = require('./routes/book');

const config = require('./config');


const app = new Koa();
const router = Router();

app.use(BodyParser());
app.use(Logger());
app.use(Cors());
app.use(error);

app.use(books).use(router.allowedMethods());

const port = config.server.port || 3000

app.listen(port, () => console.log(`listening on ${port}`));
